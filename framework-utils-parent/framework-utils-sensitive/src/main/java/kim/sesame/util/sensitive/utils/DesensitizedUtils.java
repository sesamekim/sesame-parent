package kim.sesame.util.sensitive.utils;

import cn.hutool.core.util.StrUtil;

public class DesensitizedUtils {
    public DesensitizedUtils() {
    }

    public static String desValue(String origin, int prefixNoMaskLen, int suffixNoMaskLen, String maskStr) {
        if (origin == null) {
            return null;
        } else {
            StringBuilder sb = new StringBuilder();
            int i = 0;

            for(int n = origin.length(); i < n; ++i) {
                if (i < prefixNoMaskLen) {
                    sb.append(origin.charAt(i));
                } else if (i > n - suffixNoMaskLen - 1) {
                    sb.append(origin.charAt(i));
                } else {
                    sb.append(maskStr);
                }
            }

            return sb.toString();
        }
    }

    public static String chineseName(String fullName) {
        return fullName == null ? null : desValue(fullName, 0, 1, "*");
    }

    public static String merchantName(String merchantName) {
        if (merchantName == null) {
            return null;
        } else if (merchantName.length() == 1) {
            return merchantName;
        } else if (merchantName.length() == 2) {
            return desValue(merchantName, 1, 0, "*");
        } else if (merchantName.length() != 3 && merchantName.length() != 4) {
            return merchantName.length() == 5 ? desValue(merchantName, 2, 0, "*") : desValue(merchantName, 2, merchantName.length() - 5, "*");
        } else {
            return desValue(merchantName, 1, 1, "*");
        }
    }

    public static String idCardNum(String id) {
        return desValue(id, 6, 4, "*");
    }

    public static String fixedPhone(String num) {
        return desValue(num, 0, 4, "*");
    }

    public static String mobilePhone(String num) {
        return desValue(num, 3, 4, "*");
    }

    public static String address(String address) {
        return desValue(address, 6, 0, "*");
    }

    public static String email(String email) {
        if (email == null) {
            return null;
        } else {
            int index = StrUtil.indexOf(email, '@');
            if (index <= 1) {
                return email;
            } else {
                String preEmail = desValue(email.substring(0, index), 1, 0, "*");
                return preEmail + email.substring(index);
            }
        }
    }

    public static String bankCard(String cardNum) {
        return cardNum.length() >= 12 ? desValue(cardNum, 6, 4, "*") : desValue(cardNum, 0, 4, "*");
    }

    public static String password(String password) {
        return password == null ? null : "******";
    }

    public static String key(String key) {
        if (key == null) {
            return null;
        } else {
            int viewLength = 6;
            StringBuilder tmpKey = new StringBuilder(desValue(key, 0, 3, "*"));
            if (tmpKey.length() > viewLength) {
                return tmpKey.substring(tmpKey.length() - viewLength);
            } else if (tmpKey.length() >= viewLength) {
                return tmpKey.toString();
            } else {
                int buffLength = viewLength - tmpKey.length();

                for(int i = 0; i < buffLength; ++i) {
                    tmpKey.insert(0, "*");
                }

                return tmpKey.toString();
            }
        }
    }

    public static String enterpriseName(String enterpriseName) {
        if (StrUtil.isBlank(enterpriseName)) {
            return null;
        } else if (enterpriseName.length() == 2) {
            return desValue(enterpriseName, 1, 0, "*");
        } else {
            return enterpriseName.length() > 2 ? desValue(enterpriseName, 1, 1, "*") : "*";
        }
    }
}
