package kim.sesame.util.excel.define;

public interface IExcelDictService {
    /**
     * 获取字典数据
     * @param dictTypeKey 字典类型
     * @return 字典数据
     */
    String[] getDictData(String dictTypeKey);
}
