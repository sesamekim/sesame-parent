package kim.sesame.util.sensitive.sensitive;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 脱敏字段注解(从明文字段自动获取值脱敏)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Insensitive {

    String srcField() default "";

    SensitiveTypeEnum type();
}
