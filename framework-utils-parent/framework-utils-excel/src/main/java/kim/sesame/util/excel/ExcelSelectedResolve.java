package kim.sesame.util.excel;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import kim.sesame.util.excel.annotation.ExcelSelected;
import kim.sesame.util.excel.define.IExcelDictService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class ExcelSelectedResolve {
    /**
     * 下拉内容
     */
    private String[] source;

    /**
     * 设置下拉框的起始行，默认为第二行
     */
    private int firstRow;

    /**
     * 设置下拉框的结束行，默认为最后一行
     */
    private int lastRow;

    public String[] resolveSelectedSource(ExcelSelected excelSelected) {
        if (excelSelected == null) {
            return null;
        }

        // 获取固定下拉框的内容
        String[] source = excelSelected.source();
        if (source.length > 0) {
            return source;
        }

        String dictTypeKey = excelSelected.dictTypeKey();
        if (StrUtil.isNotBlank(dictTypeKey)) {
//            return ExcelSelectFactory.getSelectData(enumTypeKey);
            IExcelDictService excelDictService = SpringUtil.getBean(IExcelDictService.class);
            if (excelDictService == null) {
                log.error("未找到IExcelDictService的实现类");
                return null;
            } else {
                return excelDictService.getDictData(dictTypeKey);
            }
        }

        return null;
    }
}