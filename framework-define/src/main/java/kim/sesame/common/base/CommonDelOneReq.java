package kim.sesame.common.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "公共删除单个请求类")
public class CommonDelOneReq<T> extends BaseUserRequest {

    @ApiModelProperty(value = "ids", required = true)
    private T id;

}