package kim.sesame.common;

public interface LogData {
    String TRACE_ID = "traceId";
    String SPAN_ID = "spanId";
}
