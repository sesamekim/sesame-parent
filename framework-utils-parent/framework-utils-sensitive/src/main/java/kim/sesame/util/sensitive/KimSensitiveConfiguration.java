package kim.sesame.util.sensitive;

import kim.sesame.util.sensitive.interceptor.PrivacyInputInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 自动扫描
 */
@Configuration
@ComponentScan
public class KimSensitiveConfiguration {

    @Bean
    public PrivacyInputInterceptor privacyInputInterceptor() {
        return new PrivacyInputInterceptor();
    }

}
