### excel 导出模板中单元格带 下拉框 的使用方法

#### 1. 直接写在注解上
```java
    @ColumnWidth(25)
    @ExcelProperty(value = "标题A")
    @ExcelSelected(source = {"xxx", "yyy", "zzz"})
    private String namea;
```
#### 2.1 动态获取下拉框的值
#### 2.2 项目中继承 IExcelDictService 类，实现 getDictData 方法，根据 dictTypeKey 返回对应的下拉框数据
```java
import com.demo.excel.select.enums.ContractNameEnum;
import com.demo.excel.select.enums.SignSourceEnum;
import com.demo.excel.select.enums.SignTypeEnum;
import kim.sesame.util.excel.define.IExcelDictService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ExcelDictServiceImpl implements IExcelDictService {

    public static final String EXCEL_SELECT_AA = "aaa";
    public static final String EXCEL_SELECT_BB = "bb";

    @Override
    public String[] getDictData(String dictTypeKey) {
        if (EXCEL_SELECT_AA.equals(dictTypeKey)) {
            AaEnum[] values = AaEnum.values();
            String[] arr = new String[values.length];
            for (int i = 0; i < values.length; i++) {
                arr[i] = values[i].getDesc();
            }
            return arr;
        } else if (EXCEL_SELECT_BB.equals(dictTypeKey)) {
            BbEnum[] values = BbEnum.values();
            String[] arr = new String[values.length];
            for (int i = 0; i < values.length; i++) {
                arr[i] = values[i].getDesc();
            }
            return arr;
        } 
        return null;
    }
}
```

#### 3. excel 注解中的写法
```java
    @ColumnWidth(25)
    @ExcelProperty(value = "标题A")
    @ExcelSelected(dictTypeKey = ExcelDictServiceImpl.EXCEL_SELECT_AA)
    private String namea;

    @ColumnWidth(25)
    @ExcelProperty(value = "标题B")
    @ExcelSelected(dictTypeKey = ExcelDictServiceImpl.EXCEL_SELECT_BB)
    private String nameb;
```

#### 4. 导出excel
```java
String fileName = System.getProperty("user.home") + "/Downloads/select.xlsx";
Map<Integer, ExcelSelectedResolve> selectedMap = EasyExcelUtil.resolveSelectedAnnotation(ExcelSelectVo.class);

ExcelWriterBuilder write = EasyExcel.write(fileName, ExcelSelectVo.class);;
        EasyExcel.write(fileName, ExcelSelectVo.class)
                .registerWriteHandler(new SelectedSheetWriteHandler(selectedMap))
        .sheet("数据").doWrite(new ArrayList<>());
    }
```